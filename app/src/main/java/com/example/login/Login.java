package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    EditText e1,e2;
    Button b1,b2;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        db = new DatabaseHelper(this);
        e1 = (EditText)findViewById(R.id.text1);
        e2 = (EditText)findViewById(R.id.text2);
        b1 = (Button) findViewById(R.id.button1);
        b2 = (Button) findViewById(R.id.button2);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = e1.getText().toString();
                String password = e2.getText().toString();
                Boolean chkuserpass = db.userpassword(user,password);
                if (chkuserpass==true)
                    Toast.makeText(getApplicationContext(),"ล็อกอินสำเร็จ",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(),"ไม่สำเร็จ",Toast.LENGTH_SHORT).show();
            }
        });

    }
}
