package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public void sendMessage(View view) {
        EditText editText = (EditText) findViewById(R.id.eUser);
        String message = editText.getText().toString();
    }

        DatabaseHelper db;
        EditText e1, e2, e3;
        Button b1, b2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DatabaseHelper(this);
        e1=(EditText)findViewById(R.id.eUser);
        e2=(EditText)findViewById(R.id.ePassword);
        e3=(EditText)findViewById(R.id.eConPassword);
        b1=(Button)findViewById(R.id.btnRegister);

        b2=(Button)findViewById(R.id.blogin);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,Login.class);
                startActivity(i);
            }
        });

        b1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String s1 = e1.getText().toString();
                String s2 = e2.getText().toString();
                String s3 = e3.getText().toString();
                if(s1.equals("")|| s2.equals("")||s3.equals("")) {
                    Toast.makeText(getApplicationContext(), "กรุณากรอกข้อมูล", Toast.LENGTH_SHORT).show();
                }
                else{
                    if (s2.equals(s3)) {
                        Boolean chkuser = db.chkuser(s1);
                        if (chkuser == true) {
                            Boolean insert = db.insert(s1, s2);
                            if (insert == true) {
                                Toast.makeText(getApplicationContext(), "ลงทะเบียนเรียบร้อย", Toast.LENGTH_SHORT).show();


                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "User Already exit", Toast.LENGTH_SHORT).show();

                        }
                    }

                }
            }
        });

    }
}
