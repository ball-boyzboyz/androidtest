package com.example.login.Screen

import com.agoda.kakao.edit.KEditText
import com.agoda.kakao.text.KButton
import com.example.login.MainActivity
import com.example.login.R

object RegisterScreen : KScreen<RegisterScreen>() {

    override val layoutId: Int? = R.layout.activity_main
    override val viewClass: Class<*>? = MainActivity::class.java

    val edit = KEditText { withId(R.id.eUser) }
    val button1 = KButton {withId(R.id.button1)}

}