package com.example.login.register_test

import android.Manifest
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.example.login.MainActivity
import com.example.login.Screen.RegisterScreen
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase

import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class RegisterTest : TestCase() {


    @get:Rule
    val runtimePermissionRule: GrantPermissionRule = GrantPermissionRule.grant(
//            Manifest.permission.WRITE_EXTERNAL_STORAGE,
//            Manifest.permission.READ_EXTERNAL_STORAGE
    )

    @get:Rule
    val activityTestRule = ActivityTestRule(MainActivity::class.java, true, false)

    @Test
    fun test() =
            run {
                step("Open Simple Screen") {
                    activityTestRule.launchActivity(null)
                    device.screenshots.take("Additional_screenshot")
                    RegisterScreen {
                        button1 {
                            isVisible()
                            click()
                        }
                    }
                }
                step("Check all possibilities of edit") {
                    scenario(
                            CheckEditScenario()
                    )
                }
            }
}