package com.example.login.register_test


import com.example.login.Screen.RegisterScreen
import com.kaspersky.kaspresso.testcases.api.scenario.BaseScenario
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext


class CheckEditScenario<ScenarioData> : BaseScenario<ScenarioData>() {

    override val steps: TestContext<ScenarioData>.() -> Unit = {
        step("Change the text in edit and check it") {
            RegisterScreen {
                edit {
                    clearText()
                    typeText("test")
                    hasText("test")
                }
            }
        }
    }
}
